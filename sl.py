import json
from flask import Flask, request
from usersl import addCertificateToDb, addCourseToDb, addSubscriptionForUser, addUser, checkUserstatus, createDatabase, deleteSubscriptionStatus, getCertificationFromDb, getCourseByName, getOldPasswordByUser, getUserProfile, subscriptionStatusOfUser, updateSubscription, updateUserPasswordByEmail, userLogin

app = Flask(__name__)
 
@app.route('/')
def hello_world():
    return 'Hello World'

@app.route('/login',methods=['POST'])
def usersLogin():
    data = json.loads(request.data)
    if data['email']==None or data['password']==None:
        return 'Need Email and password in request body', 400
    
    if not checkUserstatus(data['email']):
        return 'User Not found', 404
    elif userLogin(data['email'], data['password']):
        return 'Successfully logged', 200
    else:
        return 'Unauthorized', 401

@app.route('/register',methods=['POST'])
def usersRegister():
    data = json.loads(request.data)
    if data['email']==None or data['password']==None or data['firstName']==None or data['lastName']==None or data['mobileNumber']==None:
        return 'Need all fields in request body', 400
    
    if checkUserstatus(data['email']):
        return 'User already exists', 409
    elif addUser(data['firstName'], data['lastName'], data['email'], data['mobileNumber'], data['password']):
        return 'User registration Successful', 200
    else:
        return 'Failed to register user', 500

@app.route('/profile',methods=['GET'])
def usersProfile():
    email = request.args.get('email')
    if email==None:
        return 'Email Required in path ?email=', 409
    
    if not checkUserstatus(email):
        return 'User Not found', 404
    else:
        return getUserProfile(email), 200, {"Content-Type": "application/json"}

@app.route('/addsubscription',methods=['POST'])
def addSubscription():
    email = request.args.get('email')
    if email==None:
        return 'Email Required in path ?email=', 409

    data = json.loads(request.data)
    if data['type']==None:
        return 'Invalid request body need type in body', 400
    
    if addSubscriptionForUser(email, data['type']):
        return 'Successfully Added subscription', 200
    else:
        return 'Failed to add', 409

@app.route('/updateSubscriptionStatus',methods=['POST'])
def updateSubscriptionStatus():
    email = request.args.get('email')
    if email==None:
        return 'Email Required in path ?email=', 409
    
    data = json.loads(request.data)
    if data['type']==None:
        return 'Invalid request body need type in body', 400
    
    if updateSubscription(email, data['type'], "true"):
        return 'Successfully Updated', 200
    else:
        return 'Failed to update', 409

@app.route('/subscriptionStatus',methods=['GET'])
def getSubscriptionStatus():
	email = request.args.get('email')
	if email==None:
		return 'Email Required in path ?email=', 409
	
	if subscriptionStatusOfUser(email):
		return 'Have active Subscription', 200
	else:
		return 'Do not have any active subscription', 404

@app.route('/deleteSubscription',methods=['DELETE'])
def deleteSubscription():
    email = request.args.get('email')
    if email==None:
        return 'Email Required in path ?email=', 409

    if deleteSubscriptionStatus(email):
        return 'Successfully Deleted', 200
    else:
        return 'Failed to delete', 409

@app.route('/changepassword',methods=['POST'])
def changePassword():
    email = request.args.get('email')
    if email==None:
        return 'Email Required in path ?email=', 409

    data = json.loads(request.data)
    if data['oldpassword']==None or data['newpassword']==None or data['confirmpassword']==None:
        return 'Old and new password data is empty', 409

    if not getOldPasswordByUser(email, data['oldpassword']):
        return 'Old Password is wrong', 409

    if data['newpassword'] != data['confirmpassword']:
        return 'New Password and confirm password are not same', 400

    if updateUserPasswordByEmail(email, data['newpassword']):
        return 'Successfully Updated..', 200
    else:
        return 'Failed to update Password', 409

@app.route('/addCourse',methods=['POST'])
def addCourse():
    data = json.loads(request.data)
    if addCourseToDb(data['name'], data['description'], data['premiumStatus']):
        return 'Successfully Added', 200
    else:
        return 'Failed to add', 409

@app.route('/getCourse',methods=['GET'])
def getCourse():
    name = request.args.get('name')
    if name==None:
        return 'name Required in path ?name=', 409
    return getCourseByName(name), 200

@app.route('/addCertification',methods=['POST'])
def addCertification():
    data = json.loads(request.data)
    if addCertificateToDb(data['courseId'], data['email'], data['description']):
        return 'Successfully Added', 200
    else:
        return 'Failed to add', 409

@app.route('/getCertification',methods=['GET'])
def getCertification():
    email = request.args.get('email')
    if email==None:
        return 'email Required in path ?email=', 409
    return getCertificationFromDb(email), 200


if __name__ == '__main__':
    createDatabase()
    app.run()
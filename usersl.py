import json
import sqlite3
import uuid

def dbConnect():
    return sqlite3.connect('users.db')

def createDatabase():
    db = dbConnect()
    cursor = db.cursor()
    cursor.execute('create table if not exists users(firstName text, lastName text, email text, mobileNumber text, password text)')
    cursor.execute('create table if not exists subscriptions(email text, type text, status text)')
    cursor.execute('create table if not exists courses(id text, name text, description text, coursePremium text)')
    cursor.execute('create table if not exists certificates(id text, courseId text, email text, description text)')
    db.commit()
    db.close()

def checkUserstatus(email):
    # return true if user exists else false
    db = dbConnect()
    cursor = db.cursor()
    
    cursor.execute('select * from users where email='+'\''+email+'\'')
    result = cursor.fetchall()
    db.close()
    if not result:
        return False
    else:
        return True

def userLogin(email, password):
    db = dbConnect()
    cursor = db.cursor()
    
    cursor.execute('select * from users where email='+'\''+email+'\'')
    result = cursor.fetchall()
    db.close()
    if not result:
        return False
    elif email==result[0][2] and password==result[0][4]:
        return True
    else:
        return False

def addUser(firstName, lastName, email, mobileNumber, password):
    if not checkUserstatus(email):
        db = dbConnect()
        cursor = db.cursor()
        cursor.execute("""insert into users values(?,?,?,?,?)""", (firstName, lastName, email, mobileNumber, password))
        db.commit()
        db.close()
        return True
    else:
        return False

def getUserProfile(email):
    db = dbConnect()
    cursor = db.cursor()
    
    cursor.execute('select * from users where email='+'\''+email+'\'')
    result = cursor.fetchall()
    db.close()
    if not result:
        return json.dumps({})
    else:
        return json.dumps({"firstName":result[0][0], "lastName":result[0][1], "email":result[0][2], "mobileNumber":result[0][3], "password":result[0][4] })

def getOldPasswordByUser(email, password):
    db = dbConnect()
    cursor = db.cursor()
    
    cursor.execute('select * from users where email='+'\''+email+'\'')
    result = cursor.fetchall()
    db.close()
    if not result:
        return False
    elif result[0][4]==password:
        return True
    else:
        return False

def updateUserPasswordByEmail(email, newPassword):
    db = dbConnect()
    cursor = db.cursor()
    sql = 'Update users set password ='+'\''+newPassword+'\' where email='+'\''+email+'\''
    cursor.execute(sql)
    db.commit()
    db.close()
    return True

def subscriptionStatusOfUser(email):
    db = dbConnect()
    cursor = db.cursor()
    
    cursor.execute('select * from subscriptions where email='+'\''+email+'\'')
    result = cursor.fetchall()
    db.close()
    if not result:
        return False
    elif result[0][2] == "true":
        return True
    else:
        return False

def addSubscriptionForUser(email, type):
    db = dbConnect()
    cursor = db.cursor()
    cursor.execute("""insert into subscriptions values(?,?,?)""", (email, type, "true"))
    db.commit()
    db.close()
    return True

def updateSubscription(email, type, status):
    db = dbConnect()
    cursor = db.cursor()
    sql = 'Update subscriptions set type ='+'\''+type+'\',status ='+'\''+status+'\' where email='+'\''+email+'\''
    cursor.execute(sql)
    db.commit()
    db.close()
    return True

def deleteSubscriptionStatus(email):
    db = dbConnect()
    cursor = db.cursor()
    sql = 'Update subscriptions set status ='+'\''+"false"+'\' where email='+'\''+email+'\''
    cursor.execute(sql)
    db.commit()
    db.close()
    return True

def addCourseToDb(name, description, premiumStatus):
    db = dbConnect()
    cursor = db.cursor()
    cursor.execute("""insert into courses values(?,?,?,?)""", (str(uuid.uuid4()), name, description, premiumStatus))
    db.commit()
    db.close()
    return True

def getAllCourses():
    db = dbConnect()
    cursor = db.cursor()
    
    cursor.execute('select * from courses')
    result = cursor.fetchall()
    db.close()
    res = []
    if result == []:
        return json.dumps({})
    for r in result:
        res.append({"courseId":r[0], "name":r[1], "description":r[2], "coursePremium":r[3]})
    return json.dumps(res)

def getCourseByName(name):
    db = dbConnect()
    cursor = db.cursor()
    
    cursor.execute('select * from courses where name='+'\''+name+'\'')
    result = cursor.fetchall()
    db.close()
    res = []
    if result == []:
        return json.dumps({})
    for r in result:
        res.append({"courseId":r[0], "name":r[1], "description":r[2], "coursePremium":r[3]})
    return json.dumps(res)

def addCertificateToDb(courseId, email, description):
    db = dbConnect()
    cursor = db.cursor()
    cursor.execute("""insert into certificates values(?,?,?,?)""", (str(uuid.uuid4()), courseId, email, description))
    db.commit()
    db.close()
    return True

def getCertificationFromDb(email):
    db = dbConnect()
    cursor = db.cursor()
    
    cursor.execute('select * from certificates where email='+'\''+email+'\'')
    result = cursor.fetchall()
    db.close()
    res = []
    if result == []:
        return json.dumps({})
    for r in result:
        res.append({"certificateId":r[0], "courseId":r[1], "email":r[2], "description":r[3]})
    return json.dumps(res)


